import re

from collection_exporter.utils import get_file_stream


class CollectionExtrator:
    _BEATMAP_CHECKSUM_SIZE = 32

    def __init__(self, collectiondb_filepath):
        self.collectiondb = collectiondb_filepath

    def get_collections(self):
        return self.__parse_collections(get_file_stream(self.collectiondb))

    def __parse_collections(self, stream):
        collection = None

        for collection_or_beatmap in self.__clean_stream(stream):
            if len(collection_or_beatmap) == self._BEATMAP_CHECKSUM_SIZE:
                beatmap = collection_or_beatmap
                collection["beatmaps"].append(beatmap)
            else:
                if collection:
                    yield collection

                collection_name = collection_or_beatmap
                collection = {"name": collection_name, "beatmaps": []}

        yield collection

    def __clean_stream(self, stream):
        extract_byte_content = "b.(.*).$"
        vertical_tab = "\\x0b"
        hex_values = r"(\\x.{2})"

        content = re.compile(extract_byte_content).findall(str(stream))[0]
        stream_arr = content.split(vertical_tab)

        for line in stream_arr[1:]:
            if line:
                yield re.sub(hex_values, "", line).strip()
