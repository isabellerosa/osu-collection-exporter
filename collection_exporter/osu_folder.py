import re
import logging

from os import listdir


class OsuFolder:
    def __init__(self, osu_folder_path):
        self.osu_folder = osu_folder_path

    def get_beatmaps(self):
        for beatmap_folder in listdir(fr"{self.osu_folder}/Songs"):
            try:
                beatmap_id, artist, song = self.extract_from_folder_name(beatmap_folder)
                yield {
                    "id": beatmap_id,
                    "artist": artist,
                    "title": song,
                    "original": beatmap_folder,
                    "path": f"{self.osu_folder}/Songs/{beatmap_folder}",
                }
            except TypeError:
                logging.error(f"Couldn't extract information from: {beatmap_folder}")

    def get_difficulties(self, beatmap):
        beatmap_folder_path = fr"{self.osu_folder}/Songs/{beatmap}"
        for file in listdir(beatmap_folder_path):
            difficulty_path = fr"{beatmap_folder_path}/{file}"
            if file.endswith(".osu"):
                file = file.replace(".osu", "")
                try:
                    artist, song, mapper, difficulty = self.extract_from_beatmap_name(
                        file
                    )
                    yield {
                        "difficulty": difficulty,
                        "artist": artist,
                        "title": song,
                        "mapper": mapper,
                        "original": difficulty,
                        "path": difficulty_path,
                    }
                except TypeError:
                    logging.error(
                        f"An error occured while extracting info from difficulty: {file}"
                    )

    @staticmethod
    def extract_from_folder_name(folder):
        """
        Folder name follow the template: <beatmap id> <artist> - <song title>
        """
        regex_template = r"(\d*) (.*) - (.*)$"

        if re.match(regex_template, folder):
            return re.search(regex_template, folder).groups()
        else:
            return None

    @staticmethod
    def extract_from_beatmap_name(beatmap_name):
        """
        Beatmaps naming follow the template: <artist> - <song title> (<mapper>) [<difficulty>]
        """
        regex_template = r"(.*) - (.*) \((.*)\) \[(.*)\]$"

        if re.match(regex_template, beatmap_name):
            return re.search(regex_template, beatmap_name).groups()
        else:
            return None
