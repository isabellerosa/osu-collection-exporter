def get_incorrect_beatmap_entries():
    return [
        "",
        "384284+JJ+Lin+-+Jiang+Nan",
        "file.yaml",
        "7061asd50 REOL - Endless Line",
        ".784545 Re@asd - Endless Line",
        "REOL - Endless Line",
    ]


def get_incorrect_difficulty_entries():
    return [
        "Camellia - KillerBeast (Anythin) [Reform's.osu",
        "Camellia - KillerBeast (Anything) [Inside] (Mir).osu",
        "Camellia - KillerBeast [Insane].osu",
        "Camellia - KillerBeast (@) [B] (.*) (anyone) [HARD]",
    ]
