from unittest.mock import patch

import pytest
from collection_exporter.test.fixtures import *
from collection_exporter.osu_folder import OsuFolder


@patch("collection_exporter.osu_folder.listdir", return_value=["706150 REOL - Endless Line"])
def test_get_beatmaps_with_correct_folder_pattern(_):
    beatmap = next(OsuFolder("foo/bar").get_beatmaps())

    assert beatmap["id"] == "706150"
    assert beatmap["artist"] == "REOL"
    assert beatmap["title"] == "Endless Line"
    assert beatmap["original"] == "706150 REOL - Endless Line"


@pytest.mark.parametrize("incorrect_pattern", get_incorrect_beatmap_entries())
@patch("collection_exporter.osu_folder.listdir")
def test_get_beatmaps_with_incorrect_folder_pattern(mock_listdir, incorrect_pattern):
    mock_listdir.return_value = [incorrect_pattern]

    with pytest.raises(StopIteration):
        next(OsuFolder("foo/bar").get_beatmaps())


@patch(
    "collection_exporter.osu_folder.listdir",
    return_value=["Camellia - KillerBeast (Mir) [Hard].osu"],
)
def test_get_difficulties_with_correct_pattern(_):
    difficulty = next(OsuFolder("foo/bar").get_difficulties("beatmap"))
    assert difficulty["artist"] == "Camellia"
    assert difficulty["title"] == "KillerBeast"
    assert difficulty["mapper"] == "Mir"
    assert difficulty["difficulty"] == "Hard"


@pytest.mark.parametrize("incorrect_pattern", get_incorrect_difficulty_entries())
@patch("collection_exporter.osu_folder.listdir")
def test_get_difficulties_with_incorrect_pattern(mock_listdir, incorrect_pattern):
    mock_listdir.return_value = [incorrect_pattern]

    with pytest.raises(StopIteration):
        next(OsuFolder("foo/bar").get_difficulties("beatmap"))
