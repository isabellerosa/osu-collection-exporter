import hashlib


def get_file_stream(filepath):
    with open(filepath, "rb") as reader:
        return reader.read()


class ChecksumHandler:
    @staticmethod
    def get_file_checksum(filepath):
        hash_generator = hashlib.md5()

        hash_generator.update(get_file_stream(filepath))

        return hash_generator.hexdigest()

    @staticmethod
    def validate_file_checksum(filepath, checksum):
        return ChecksumHandler.get_file_checksum(filepath) == checksum
